/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaconsole;
import java.util.Scanner;
import java.util.Date;
import java.io.Console;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author p.vasin
 */
public class JavaConsole {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        JavaConsole.var06(args);
    }
    
    public static void var06(String[] args) throws IOException {
        //------
        //с объектом типа File ассоциируется файл на диске
        File fp = new File( "file\\inroot\\folder\\example.txt" );
        // другие способы создания объекта
        //File fp = new File("\\com\\learn", "FileTest.java");
        //File fp=new File("d:\\temp\\demo.txt");
        //File fp=new File("demo.txt");        
        if (fp.isFile()) {
            System.out.println("Имя файла:\t" + fp.getName());
            System.out.println("Путь к файлу:\t" + fp.getPath());
            System.out.println("Абсолютный путь:\t" + fp.getAbsolutePath());
            System.out.println("Канонический путь:\t" + fp.getCanonicalPath());
            System.out.println("Размер файла:\t" + fp.length());
            System.out.println("Последняя модификация файла:\t" + fp.lastModified());
            System.out.println("Файл доступен для чтения:\t" + fp.canRead());
            System.out.println("Файл доступен для записи:\t" + fp.canWrite());
            System.out.println("Файл удален:\t" + fp.delete());            
        }
        
        if(fp.createNewFile()){
            System.out.println("Файл " + fp.getName() + " создан");
        }
        
        if(fp.exists()) {
            System.out.println("temp файл "+ fp.getName() + " существует");
        } else {
            System.out.println("temp файл " + fp.getName() + " не существует");
        }
        
        File dir = new File( "com\\learn" );
        if (dir.isDirectory()) {
            System.out.println("Директория!");
        }        

        if(dir.exists()) {
            System.out.println( "Dir " + dir.getName() + " существует " );
            File [] files = dir.listFiles();
            System.out.println("");
            for(int i=0; i < files.length; i++){
                Date date = new Date(files[i].lastModified());
                System.out.println(files[i].getPath() + " \t| " + files[i].length() + "\t| " + date.toString());
            }
        }        
    }    
    
    // 5. -----------
    public static void var05(String[] args) {         
        // получаем консоль
        Console console = System.console();
        if(console!=null){
            // считываем данные с консоли
            String login = console.readLine("Введите логин:");
            char[] password = console.readPassword("Введите пароль:");             
            console.printf("Введенный логин: %s \n", login);
            console.printf("Введенный пароль: %s \n", new String(password));
        }
    }    
    
    // 4. -----------
    public static void var04(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s1, s2;
        s1 = sc.nextLine();
        s2 = sc.nextLine();
        System.out.println(s1 + s2);
    }    
    
    // 3. ------------
    public static void var03(String[] args) {
        Scanner sc = new Scanner(System.in);
        double  i = sc.nextDouble(); // если ввести букву s, то случится ошибка во время исполнения
        System.out.println(i/3);
    }    
    
    // 2.--------------
    
    private static void var02(String[] args) {
        Scanner sc = new Scanner(System.in); // создаём объект класса Scanner
        int i = 2;
        System.out.print("Введите целое число: ");
        if(sc.hasNextInt()) { // возвращает истинну если с потока ввода можно считать целое число
          i = sc.nextInt(); // считывает целое число с потока ввода и сохраняем в переменную
          System.out.println(i*2);
        } else {
          System.out.println("Вы ввели не целое число");
        }        
    }
    
    // 1.---------------
    private static void var01(String[] args) {
        int playerNumber;
        int number = Integer.MAX_VALUE;
        String answer;
        Scanner sc = new Scanner(System.in);
        System.out.println("Input you number");
        playerNumber = sc.nextInt();
        while (true) {
            number = (int)(Math.random()*10000);
            System.out.println(number + " Right?");
            answer = sc.next();
            if (answer.equalsIgnoreCase("yes")) {
                System.out.println("you win");
                break;
            }
        }
    }
    
}
